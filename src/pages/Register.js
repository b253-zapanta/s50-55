import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {
  const { user } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  const [registerSuccess, setRegisterSuccess] = useState(false);

  console.log(email);
  console.log(firstName);
  console.log(lastName);
  console.log(mobileNo);
  console.log(password1);
  console.log(password2);

  function registerUser(e) {
    e.preventDefault();

    // console.log(email);
    fetch("http://localhost:4000/users/checkEmail", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Email already exists.",
          });
        } else {
          fetch("http://localhost:4000/users/register", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              password: password1,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              // console.log(data);
              if (data) {
                Swal.fire({
                  title: "Registration Successful",
                  icon: "success",
                  text: "Welcome to Zuitt!",
                });
                setRegisterSuccess(true);
              } else {
                Swal.fire({
                  title: "Registration Failed",
                  icon: "error",
                  text: "Please contact us for this issue!",
                });
              }
            });
        }
      });
  }

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      mobileNo !== "" &&
      email !== "" &&
      password1 !== "" &&
      mobileNo.length >= 11 &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password1, password2, firstName, lastName, mobileNo]);

  return registerSuccess !== false ? (
    <Navigate to="/login" />
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group className="mb-3" controlId="userFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="string"
          placeholder="Enter first name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="string"
          placeholder="Enter last name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter your email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="string"
          placeholder="Enter mobile #"
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}

// Instructions that can be provided to the students for reference:
// Activity:
// 1. Refactor the Register page to include additional form inputs for the user's first name, last name and mobile number.
// 2. Refactor the useEffect hook for the form validation to include the user’s first name, last name and mobile number with at least 11 digits.
// 3. Make the register feature work that will prevent the user from registering if the email already exists.
// 4. Redirect the user to the Login page upon successful registration.
// 5. Create a git repository named S55.
// 6. Add another the remote link and push to git with the commit message of Add activity code - S49.
// 7. Add the link in Boodle.
